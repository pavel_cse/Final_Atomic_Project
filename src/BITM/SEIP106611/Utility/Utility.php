<?php
    namespace App\BITM\SEIP106611\Utility;
    class Utility{
        static public function d($param=false){
            echo "<pre>";
            var_dump($param);
            echo "</pre>";
        }   
        static public function dd ($param=false){
            self::d($param);
            die();
        }
		static public function redirect($url="/FinalAtomicProject/Views/SEIP106611/Book/index.php"){
			header("Location:".$url);
    }
    }
?>
