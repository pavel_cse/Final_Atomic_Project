<?php
    //ini_set('display_errors','Off');
    include_once("../../../vendor/autoload.php");
    
    use \App\BITM\SEIP106611\Company\Company;
    
    $company = new Company();
    $descriptions = $company->index();
    
    
    
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Atomic Project</title>
        <!-- Bootstrap -->
        <link href="../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../Resource/css/style.css" rel="stylesheet">
        
    </head>
    <body>
        <div class="container">
            <h1>List Of Company's</h1>
               <span class="btn btn-info ">Search/Filter</span>  <span  class="btn btn-success" id="utility">Download As: PDF | XL</span><br><br>
            <table  id="bg" class="table table-bordered table-condensed table-striped text-center">
                <thead>
                    <tr class="success">
                        <th style="text-align: center;color:#D34A24;font-size: 18px">Sl.</th>
                        <th style="text-align: center;color:#D34A24;font-size: 18px">Company Name &dArr;</th>
						<th style="text-align: center;color:#D34A24;font-size: 18px">Description</th>
                        <th style="text-align: center;color:#D34A24;font-size: 18px">Action</th>
                    </tr>
                </thead>
                <tbody>
				   <?php
				   $slno =1;
				   foreach($descriptions as $company){
				   ?>
					<tr>
						<td><?php echo $slno;?></td>
						
						<td><a href="#"><?php echo $company->c_name;?></a></td>
						<td><?php echo $company->description;?></td>
						<td><a class="btn btn-success" href="">View</a>  <a class="btn btn-info" href="">Edit</a>  <a class="btn btn-danger" href="">Delete</a>  <a class="btn btn-warning" href="">Trash/Recover</a>  <a class="btn btn-primary" href="">Email to Friend</a></td>
					</tr>
					<?php
					$slno++;
					}
					?>
            </tbody>
            </table>
           
            <a href="create.php"><button type="button" class="btn btn-primary">Add New</button></a>

            <div id="pagi">
                Prev &nbsp;&nbsp;1 | 2 | 3 &nbsp;&nbsp; Next
            </div>
            <div>
               <nav style="float:right">
                    <li><a href="javascript:history.go(-2)">Go To Project Page</a></li>
					<li><a href="../../../index.php">Go To Home </a></li>
               </nav>
            </div>
        </div>
    </body>
</html>

   